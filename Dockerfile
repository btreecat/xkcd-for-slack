FROM python:slim

WORKDIR /opt/x4s/
EXPOSE 8000

# Setup internal venv to be more like development and better isolated from OS packages
ENV VIRTUAL_ENV=/opt/units/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN python -m venv $VIRTUAL_ENV

COPY requirements.txt /opt/x4s/

RUN pip install -r requirements.txt

COPY src/* /opt/x4s/

CMD ["uvicorn", "x4s:api", "--host", "0.0.0.0"]