import os
from typing import Any
from xkcd import getComic, getRandomComic, getLatestComic, Comic
import re
import random
import httpx

FLAGS = {"random": ["-r", "--random"], "latest": ["-l", "--latest"]}
LINK_FILTERS = ["archive", "about", "blog", "store", "what", "random", "wiki", "3d"]
API_KEY = os.environ.get('CUST_SEARCH_KEY')
SEARCH_ID = os.environ.get('SEARCH_ENGINE_ID')


def search_google(query_str):
    """
    Search google using custom engine search

    This requires an API key and a custom search engine to be created with google
    :param query_str:
    :return:

    >>> search_links = search_google("salt")
    >>> len(search_links)
    10
    >>> len([link for link in search_links if 'xkcd.com/974/' in link])
    1

    """
    url = "https://customsearch.googleapis.com/customsearch/v1"
    query_params = {'key': API_KEY,
                    'cx': SEARCH_ID,
                    'q': query_str}

    resp = httpx.get(url, params=query_params)
    if int(resp.json().get('searchInformation').get('totalResults')) > 0:
        results = resp.json().get('items')
    else:
        results = []
    links = [result.get('link') for result in results]
    return links


def check_flag(token: str) -> bool:
    """
    Check if a token matches one of the flags
    :param token: str
    :return: bool

    >>> check_flag('-r')
    True
    >>> check_flag('--latest')
    True
    >>> check_flag('-q')
    False
    >>> check_flag('search')
    False

    """
    found = False
    for key in FLAGS:
        if token in FLAGS[key]:
            found = True
    return found


def flagged_commands(command_tokens: list) -> dict[str, str | None | Any]:
    """
    Process commands that have been detected to be part of the flagged list
    :param command_tokens:
    :return:

    >>> comic_data = flagged_commands(['-r'])
    >>> comic_data['comic'] is not None
    True
    >>> comic_data['msg'] is not None
    True

    """
    if command_tokens[0] in FLAGS['random']:
        msg = f"Fetching Random: {command_tokens}"
        comic = getRandomComic()
    else:
        msg = f"Fetching Latest: {command_tokens}"
        comic = getLatestComic()

    return {"comic": comic, "msg": msg}


def filter_links(results: list) -> list:
    """
    Filter links that are unrelated
    :param results:
    :return:

    >>> result_list = ['https://store.xkcd.com/password', \
                    'https://blog.xkcd.com/2010/05/03/color-survey-results/', \
                    'https://what-if.xkcd.com/', \
                    'https://xkcd.com/archive/', \
                    'https://c.xkcd.com/random/comic/', \
                    'https://xkcd.com/about/', \
                    'https://xkcd.com/455/']
    >>> filter_links(result_list)
    ['https://xkcd.com/455/']

    """
    filtered = []
    for result in results:
        flag = True
        for link_filter in LINK_FILTERS:
            if link_filter in result:
                flag = False

        if flag:
            filtered.append(result)
    return filtered


def comic_id(link: str) -> str:
    """
    Parse the comic ID from the link
    :param link:
    :return:

    >>> comic_id('https://xkcd.com/20/')
    '20'

    """
    p = re.compile(r'/(\d+)/')
    search_match = p.search(link)
    if search_match is not None:
        group = search_match.groups()[0]
    else:
        group = None
    return group


async def keyword_search(command_tokens: list) -> Comic:
    """
    Search for a comic given one or more keywords
    :param command_tokens:
    :return:

    >>> import asyncio
    >>> comic_data = asyncio.run(keyword_search(['make', 'sandwich']))
    >>> comic_data['comic'] is not None
    True
    >>> comic_data['msg'] is not None
    True

    """
    spin = False
    if command_tokens[-1] in FLAGS['random']:
        command_tokens.pop()
        spin = True
    q = " ".join(command_tokens)
    search_links = search_google(q)
    # filtered = filter_links(search_links)
    if len(search_links) > 0:
        if spin:
            # avoid the first, look for a diff option
            link_idx = random.randrange(0, len(search_links))
            c_id = comic_id(search_links[link_idx])
        else:
            # just take the first
            c_id = comic_id(search_links[0])
        if c_id is not None:
            msg = f"Searching Keywords: {q}"
            comic = getComic(c_id)
        else:
            comic = getRandomComic()
            msg = f"No Results, Random Comic Instead: {q}"

    else:
        comic = getRandomComic()
        msg = f"No Results, Random Comic Instead: {q}"
    return {"comic": comic, "msg": msg}


async def fetch_comic(command_tokens: list) -> dict[str, str | None | Any]:
    """
    Given a set of command tokens, figure out which comic to fetch
    :param command_tokens:
    :return:

    >>> import asyncio
    >>> comic_data = asyncio.run(fetch_comic(['-r'])) # test random
    >>> comic_data['comic'] is not None
    True
    >>> comic_data['msg'] is not None
    True
    >>> comic_data['spin']
    False
    >>> comic_data = asyncio.run(fetch_comic(['--latest'])) # Test latest
    >>> comic_data['comic'] is not None
    True
    >>> comic_data['msg'] is not None
    True
    >>> comic_data['spin']
    False
    >>> comic_data = asyncio.run(fetch_comic(['2559']))
    >>> comic_data['comic'].number
    2559
    >>> comic_data['msg'] is not None
    True
    >>> comic_data['spin']
    False
    >>> comic_data = asyncio.run(fetch_comic(['facebook']))
    >>> comic_data['comic'] is not None
    True
    >>> comic_data['msg'] is not None
    True
    >>> comic_data['spin']
    True
    >>> comic_data = asyncio.run(fetch_comic([]))
    >>> comic_data['comic'] is not None
    True
    >>> comic_data['msg']
    'Fetching Random: No command'
    >>> comic_data['spin']
    True

    """
    data = {"comic": None, "msg": None, "spin": False}
    if len(command_tokens) > 0:
        if command_tokens[0].isdigit():
            data["msg"] = f"Fetching Comic: {command_tokens[0]}"
            data["comic"] = getComic(command_tokens[0])
        elif check_flag(command_tokens[0]):
            data = flagged_commands(command_tokens)
            data['spin'] = False
        else:
            # Time to search
            data = await keyword_search(command_tokens)
            data['spin'] = True
    else:
        data["msg"] = "Fetching Random: No command"
        data["comic"] = getRandomComic()
        data['spin'] = True

    return data


def build_message(comic, *, user=False, visible=False, query_str=None, actions=True, spin=False, delete_og=False,
                  replace_og=False) -> dict:
    """
    Build slack message

    :param user:
    :param comic:
    :param visible:
    :param query_str:
    :param actions:
    :param spin:
    :param delete_og:
    :param replace_og:
    :return:

    >>> a_comic = getComic("1373")
    >>> a_msg = build_message(a_comic)
    >>> a_msg.get('blocks') is not None
    True
    >>> len(a_msg.get('blocks'))
    2
    >>> a_comic.title in a_msg['blocks'][0]['title']['text']
    True
    >>> a_msg['blocks'][0]['image_url'] is not None
    True
    >>> len(a_msg.get('blocks')[1]['elements'])
    2
    >>> a_msg = build_message(a_comic, visible=True, spin=True, query_str="mom", delete_og=True)
    >>> a_msg['response_type']
    'in_channel'
    >>> a_msg['delete_original']
    True
    >>> len(a_msg.get('blocks')[1]['elements'])
    3
    >>> a_msg = build_message(a_comic, actions=False, replace_og=True)
    >>> len(a_msg.get('blocks'))
    1
    >>> a_msg['replace_original']
    True

    """
    msg = {
        "blocks": [
            {
                "type": "image",
                "title": {
                    "type": "plain_text",
                    "text": f"{comic.title}"
                },
                "image_url": f"{comic.imageLink}",
                "alt_text": f"{comic.altText}"
            }
        ]
    }
    if visible:
        msg['response_type'] = "in_channel"

    if delete_og:
        msg['delete_original'] = True

    if replace_og:
        msg['replace_original'] = True

    act = {
        "type": "actions",
        "elements": [
            {
                "type": "button",
                "action_id": "approve",
                "text": {
                    "type": "plain_text",
                    "text": "Approve"
                },
                "style": "primary",
                "value": f"{comic.number}"
            },
            {
                "type": "button",
                "action_id": "reject",
                "text": {
                    "type": "plain_text",
                    "text": "Reject"
                },
                "style": "danger",
                "value": "bobs_burgers"
            }
        ]
    }

    spin_act = {
        "type": "button",
        "action_id": "spin",
        "text": {
            "type": "plain_text",
            "text": "Spin"
        },
        "value": f"{query_str if query_str != '' else 'xkcd' }"
    }

    if actions:
        msg['blocks'].append(act)

    user_block = {
        "type": "section",
        "text": {"type": "plain_text", "text": f"Thank you {user} for your wonderful choice in comedic relief!"}
    }
    if user:
        msg['blocks'].append(user_block)

    if spin:
        msg['blocks'][1]['elements'].insert(1, spin_act)

    return msg


def tokenize(query: str) -> list:
    """
    Return a list of tokens based on a string with spaces
    :param query:
    :return:

    >>> tokens = tokenize("the quick brown fox jumped over the lazy dog")
    >>> len(tokens)
    9
    >>> tokens[0]
    'the'
    >>> tokens[-1]
    'dog'
    >>> tokenize('')
    []

    """

    return query.split(" ") if query != '' else []
