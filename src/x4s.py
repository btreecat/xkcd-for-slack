import logging
import os

from fastapi import FastAPI, Request, Response
from slack_bolt.adapter.fastapi.async_handler import AsyncSlackRequestHandler
from slack_bolt.async_app import AsyncApp

import utils

# Set log level
if os.environ.get("X4S_LOGLVL") == "DEBUG":
    logging.basicConfig(level=logging.DEBUG)

# Create out fastapi app
api = FastAPI()

# Create out slack_bolt app
app = AsyncApp()
app_handler = AsyncSlackRequestHandler(app)


@app.event("app_mention")
async def handle_app_mention(body, say, logger):
    logger.info(body)
    await say("Use `/xkcd` command to embed comics directly into your conversations!")


@app.command("/xkcd")
async def handle_xkcd_command(ack, respond, command):
    await ack()
    query = command.get('text') if command.get('text') is not None else ""
    command_tokens = utils.tokenize(query)
    comic_data = await utils.fetch_comic(command_tokens)
    await respond(comic_data['msg'])
    msg = utils.build_message(comic_data['comic'], query_str=query, spin=comic_data['spin'])
    await respond(msg)


@app.action("approve")
async def handle_approve(ack, respond, body, say):
    await ack()
    comic_id = body.get('actions')[0]['value']
    comic_data = await utils.fetch_comic([comic_id])
    user_info = await app.client.users_info(user=body['user']['id'])
    msg = utils.build_message(comic_data['comic'], user=user_info.data['user']['profile']['display_name'], visible=True, actions=False, delete_og=True)
    print(msg)
    await respond(msg)


@app.action("spin")
async def handle_spin(ack, respond, body):
    await ack()
    query_str = body.get('actions')[0]['value']
    tokens = utils.tokenize(query_str)
    tokens.append('-r')
    comic_data = await utils.fetch_comic(tokens)
    msg = utils.build_message(comic_data['comic'], query_str=query_str, spin=True, replace_og=True)
    await respond(msg)


@app.action("reject")
async def handle_cancel(ack, respond):
    await ack()
    await respond("Comic rejected", delete_original=True)


@api.post("/slack/events")
async def slack_events(req: Request) -> Response:
    # Pass the req down to the handler
    # it will dispatch correctly from there
    return await app_handler.handle(req)


@api.get("/")
async def root(req: Request) -> Response:
    return {"mesage": "Mic check 1-2 1-2"}
