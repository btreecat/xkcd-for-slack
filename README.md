# xkcd for Slack

A FOSS service that you can self-host, providing `/xkcd` commands in Slack.

## Getting started

### Requirements 

#### Deployment

- Docker (to run the service)
- Slack (to interact with the service using `/commands`)
- Google Programmable Search API Access

# Development

- Python 3.10.x
- `pip install -r requirements.txt`
- Slack
- Google Programmable Search API Access

### Sign up for Slack

You will need a `SLACK_BOT_TOKEN` and a `SLACK_SIGNING_SECRET` which you can get when you create a slack app. 

### Sign up for Custom Search

You will need a [Google Programmable search](https://programmablesearchengine.google.com/) setup, and the associated `SEARCH_ENGINE_ID` and `CUST_SEARCH_KEY`.

#### Sites to Exclude

Under the `Advanced` option for the `Programmable Search Engine` it is recommended to add the following exclusions: 

| URL                    | Setting      |
|------------------------|--------------|
| 3d.xkcd.com            | Exclude all  |
| m.xkcd.com             | Exclude all  |
| store.xkcd.com         | Exclude all  |
| wiki.xkcd.com          | Exclude all  |
| what-if.xkcd.com       | Exclude all  |
| blog.xkcd.com          | Exclude all  |
| xkcd.com/random/comic/ | Exclude just |
| xkcd.com/about/        | Exclude just |
| xkcd.com/archive/      | Exclude just |
| xkcd.com/              | Exclude just |



### Running the Service

This service needs to be hosted at a publicly reachable location, so any machine with a public IP will work. 

#### Docker

#### Building the Container

```bash
docker build -t btreecat/xkcd-4-slack .
```

#### Local Compose
There is an included `Dockerfile` for building the image and a `docker-compose.yml` for building and running the service.

```bash
docker-compose up --build
```

#### Hosted Container
Prebuilt Docker container: [btreecat/xkcd-4-slack](https://hub.docker.com/repository/docker/btreecat/xkcd-4-slack)

#### .env Key:Value

| Key                    | Description                                                        |
|------------------------|--------------------------------------------------------------------|
| `SLACK_BOT_TOKEN`      | Token from Slack for your bot authentication ( has '-' characters) |
| `SLACK_SIGNING_SECRET` | Token from Slack for your bot authentication                       |
| `CUST_SEARCH_KEY`      | Token from Google for your Custom Search Engine API Access         |
| `SEARCH_ENGINE_ID`     | ID from Google that matches your Programmable Search Engine        |

### Install the APP

[Install and configure the app here](https://api.slack.com/apps)

## Usage

The bot responds to `/xkcd` commands or to `@xkcd-4-slack` mentions (but only with helper text in a mention).

## Features

- Comic by ID
- Random Comic
- Latest Comic
- Key-word Search
- Approve-before-post Workflow

### Comic by ID

Given a comic ID number, fetch that specific comic. 

`/xkcd 386`

### Random Comic

Pass the random flag (`-r` or `--random`) to get a random comic.

`/xkcd -r`

### Latest Comic

Pass the latest flag (`-l` or `--latest`) to get the latest comic.

`/xkcd -l`

### Key-word Search

Uses DuckDuckGo to site search xkcd.com for the right comic. By default this will pick the first valid
result. Using the `-r` or `--random` flag after your search terms will force the choice to be random rather
than the first one. 

`/xkcd exploit`

or

`/xkcd sys admin -r`

### Approve-before-post Workflow

Just like Giphy, this plugin will preview the image only to the person running the `/xkcd` command. 
From the preview, you can choose to `Approve` or `Reject` the image selected.

When searching, there is also the option to select `Spin` which will re-run the search (stateless), but with the 
`-r` random flag added on to force a random choice from the results list rather than just picking the first one.

## Roadmap

- Metrics


## Testing

Using [doctest](https://docs.python.org/3/library/doctest.html) for testing.

From the repo root:
```bash
python -m doctest -v src/utils.py
```

## Authors and acknowledgment

- Stephen Tanner - Author
- Bob Vail - Motivation
- Randall Munroe - xkcd


## License

MIT

## Project status

Mostly complete. Bug fixes as needed.

